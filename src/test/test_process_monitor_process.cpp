/*!*******************************************************************************************
 *  \file       process_monitor_test_process.cpp
 *  \brief      ProcessMonitorTest implementation file.
 *  \details    This file implements the ProcessMonitorTest class.
 *  \author     Enrique Ortiz
 *  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM) 
 *
 *     This program is free software: you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation, either version 3 of the License, or 
 *     (at your option) any later version. 
 *   
 *     This program is distributed in the hope that it will be useful, 
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 *     GNU General Public License for more details. 
 *   
 *     You should have received a copy of the GNU General Public License 
 *     along with this program. If not, see http://www.gnu.org/licenses/. 
 ********************************************************************************/

 /*TODO
I can add an ip address at the roslaunch o try to obtain a true (ertting an IP known to work)
I can check for the actual supervised process to see if it is online or not
Do the same to know if it is started or not.
 */
#include "test_process_monitor_process.h"
#include "drone_process.h"  //This file is needed to obtain the State and Error defs.

ProcessMonitorTest::ProcessMonitorTest(int argc, char **argv)
{
  printf("%s\n", "Starting ProcessMonitorTest");
  ros::init(argc, argv, ros::this_node::getName());
  ros::NodeHandle n;
  
  watchdog_pub = n.advertise<droneMsgsROS::AliveSignal>("process_alive_signal", 10);
  error_pub = n.advertise<droneMsgsROS::ProcessError>("self_detected_process_error", 10);
  error_sub = n.subscribe("error_notification_topic", 1000, &ProcessMonitorTest::errorInformerCallback,this);
  
  wifiIsOkClient = n.serviceClient<std_srvs::Trigger>("wifiIsOk");
  moduleIsOnlineClient = n.serviceClient<droneMsgsROS::askForModule>("moduleIsOnline");
  moduleIsStartedClient = n.serviceClient<droneMsgsROS::askForModule>("moduleIsStarted");

  all_ok = true;
  death_detected = false;
  node_restoration = false;
  node_error_detection = false;

  char buf[32];  
  gethostname(buf,sizeof buf);  
  hostname.append(buf);
}

ProcessMonitorTest::~ProcessMonitorTest()
{
  ROS_INFO("Closing ProcessMonitorTest");
}

void ProcessMonitorTest::validate()
{
  if(test1()) //Node death detection
    test2();  //Node reinsertion
  test3();    //Node error detection
  test4();    //moduleIsOnline service.
  test5();    //moduleIsStarted service.
  test6();    //wifiIsOk service.


  if(all_ok)
      printf("\n%s\n", COLOR_GREEN"All tests PASSED. ProcessMonitor CHECKED."COLOR_RESET);
  else
    printf("\n%s\n", COLOR_RED"Process_Monitor didn't passed all tests. check FAILED."COLOR_RESET);
  //string kill_command = "rosnode kill -a";
  //system(ping_command.c_str());
}

bool ProcessMonitorTest::test1()
{
  ros::Duration(1).sleep();
  watchdog_message.current_state.state = STATE_STARTED;
  watchdog_message.process_name = ros::this_node::getName();
  watchdog_message.hostname = hostname;
  watchdog_pub.publish(watchdog_message);

  for (int i=1;i<10;i++)
  {
    ros::Duration(1).sleep();
    ros::spinOnce();
    if (death_detected)
      i=10;
  }
  if(death_detected)
    printf("%s%d%s\n",COLOR_GREEN "Test 1/",NUM_TEST,", Node death detection: PASSED" COLOR_RESET);
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 1/",NUM_TEST,", Node death detection: FAILED" COLOR_RESET);
    all_ok = false;
  }

};


void ProcessMonitorTest::test2()
{
  watchdog_message.current_state.state = STATE_STARTED;
  watchdog_message.process_name = ros::this_node::getName();
  watchdog_pub.publish(watchdog_message);

  for (int i=1;i<10;i++)
  {
    ros::Duration(1).sleep();
    ros::spinOnce();
    if (node_restoration)
      i=10;
  }

  if(death_detected)
    printf("%s%d%s\n",COLOR_GREEN "Test 2/",NUM_TEST,", Node restoration: PASSED" COLOR_RESET);
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 2/",NUM_TEST,", Node restoration: FAILED" COLOR_RESET);
    all_ok = false;
  }

};

void ProcessMonitorTest::test3()
{
  droneMsgsROS::ProcessError error_message;
  error_message.header.stamp = ros::Time::now();
  error_message.hostname = hostname;
  error_message.process_name = ros::this_node::getName();
  error_message.function = "fake function name";
  error_message.description = "This is a fake error used to test the process monitor";
  error_message.reference_code = 3;
  error_pub.publish(error_message);

  for (int i=1;i<10;i++)
  {
    ros::Duration(1).sleep();
    ros::spinOnce();
    if (node_error_detection)
      i=10;
  }

  if(node_error_detection)
    printf("%s%d%s\n",COLOR_GREEN "Test 3/",NUM_TEST,", Node error detection: PASSED" COLOR_RESET);
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 3/",NUM_TEST,", Node error detection: FAILED" COLOR_RESET);
    all_ok = false;
  }
};

void ProcessMonitorTest::test4() // moduleIsOnline service.
{
  droneMsgsROS::askForModule srv;
  srv.request.module_name = "test_process_monitor_process_main";
  bool subtestModuleIsOnline = false;
  bool subtestModuleIsOffline = false;
  death_detected = false;


  //Subtest 1
  if (moduleIsOnlineClient.call(srv)) //If call was ok.
  {
    if (srv.response.ack)  //If response is true.
    {
      subtestModuleIsOnline = true;
    }
  }
  else
  {
    printf("Can't make call on test 4");
  }

  //Subtest 2
  for (int i=1;i<10;i++)
  {
    ros::Duration(1).sleep();
    ros::spinOnce();
    if (death_detected)
      i=10;
  }
  if (moduleIsOnlineClient.call(srv)) //If call was ok.
  {
    if (!srv.response.ack)  //If response is false.
    {
      subtestModuleIsOffline = true;
    }
  }
  else
  {
    printf("Can't make call on test 4");
  }  

  if (subtestModuleIsOnline && subtestModuleIsOffline)
  {
      printf("%s%d%s\n",COLOR_GREEN "Test 4/",NUM_TEST,", ModuleIsOnline service: PASSED" COLOR_RESET);
  }
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 4/",NUM_TEST,", ModuleIsOnline service: FAILED" COLOR_RESET);
    all_ok = false;
  }
}

void ProcessMonitorTest::test5() //moduleIsStarted service.
{
  droneMsgsROS::askForModule srv;
  srv.request.module_name = "test_process_monitor_process_main";
  bool subtestModuleIsStarted = false;
  bool subtestModuleIsNotStarted = false;

  watchdog_message.current_state.state = STATE_STARTED;
  watchdog_pub.publish(watchdog_message);

  //Subtest 1
  if (moduleIsStartedClient.call(srv)) //If call was ok.
  {
    if (srv.response.ack)  //If response is true.
    {
      subtestModuleIsStarted = true;
    }
  }
  else
  {
    printf("Can't make call on test 5");
  }

  //Subtest 2
  watchdog_message.current_state.state = STATE_NOT_STARTED;
  watchdog_pub.publish(watchdog_message);
  if (moduleIsStartedClient.call(srv)) //If call was ok.
  {
    if (!srv.response.ack)  //If response is false.
    {
      subtestModuleIsNotStarted = true;
    }
  }
  else
  {
    printf("Can't make call on test 5");
  }  

  if (subtestModuleIsStarted && subtestModuleIsNotStarted)
  {
      printf("%s%d%s\n",COLOR_GREEN "Test 5/",NUM_TEST,", ModuleIsStarted service: PASSED" COLOR_RESET);
  }
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 5/",NUM_TEST,", ModuleIsStarted service: FAILED" COLOR_RESET);
    all_ok = false;
  }
}

void ProcessMonitorTest::test6() //wifiIsOk service.
{
  std_srvs::Trigger srv;
  if (wifiIsOkClient.call(srv)) //If call was ok.
  {
    printf(COLOR_ORANGE"%s%d, The wifiIsOk service response was %s \n"COLOR_RESET, "Test 6/",NUM_TEST,srv.response.success ? "true" : "false");
    printf(COLOR_ORANGE"Manually check if this is a valid response\n"COLOR_RESET);
  }
  else
  {
    printf("Can't make call on test 6");
  }

}

void ProcessMonitorTest::errorInformerCallback(const droneMsgsROS::ProcessError::ConstPtr& msg)
{
  int code = msg->reference_code;
  
  if(code==1)  //If node death detected
    death_detected = true;
  else if (code==2)  //If node restoration occured
    node_restoration = true;
  else if (code==3)  //If node restoration occured
    node_error_detection = true;
}