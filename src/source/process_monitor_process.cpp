/*!*******************************************************************************************
 *  \file       process_monitor_process.cpp
 *  \brief      ProcessMonitor implementation file.
 *  \details    This file implements the ProcessMonitor class.
 *  \author     Enrique Ortiz, David Palacios
 *  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM) 
 *
 *     This program is free software: you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation, either version 3 of the License, or 
 *     (at your option) any later version. 
 *   
 *     This program is distributed in the hope that it will be useful, 
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 *     GNU General Public License for more details. 
 *   
 *     You should have received a copy of the GNU General Public License 
 *     along with this program. If not, see http://www.gnu.org/licenses/. 
 ********************************************************************************/

#include "process_monitor_process.h"

ProcessMonitor::ProcessMonitor(int argc, char **argv)
{
  //Ros initialization
  ros::init(argc, argv, ros::this_node::getName());
  ros::NodeHandle n;
  
  //Get params
  if (!n.getParam("alive_signal_topic", alive_signal_topic))
    alive_signal_topic = "process_alive_signal";

  if (!n.getParam("error_topic", error_topic))
    error_topic = "self_detected_process_error";
  
  if (!n.getParam("error_notification_topic", error_notification_topic))
    error_notification_topic = "error_notification_topic";

  if (!n.getParam("processes_performance_topic", processes_performance_topic))
    processes_performance_topic = "processes_performance";

  if (!n.getParam("wifi_connection_topic", wifi_connection_topic))
    wifi_connection_topic = "wifiIsOk";

  if (!n.getParam("drone_ip_address", drone_ip_address))
    drone_ip_address = "192.168.1.1";

  ping_command = "ping -w 1 ";
  ping_command += drone_ip_address;
  ping_command += "> null";
  connection_status = false;

  //Set topics
  ros::Subscriber watchdog_sub = n.subscribe(alive_signal_topic, 1000, &ProcessMonitor::watchdogCallback,this);
  ros::Subscriber error_sub = n.subscribe(error_topic, 1000, &ProcessMonitor::errorCallback,this);
  error_informer = n.advertise<droneMsgsROS::ProcessError>(error_notification_topic, 10);
  process_informer = n.advertise<droneMsgsROS::ProcessDescriptorList>(processes_performance_topic, 10);
  wifi_connection_informer = n.advertise<std_msgs::Bool>(wifi_connection_topic,10);
  
  //Set services
  isConnectedSrv=n.advertiseService("wifiIsOk",&ProcessMonitor::connectionServCall,this);
  isStartedSrv=n.advertiseService("moduleIsOnline",&ProcessMonitor::moduleIsOnlineServCall,this);
  isOnlineSrv=n.advertiseService("moduleIsStarted",&ProcessMonitor::moduleIsStartedServCall,this);

  //Create child process
  pipe(pipe_handler);
  if(fork() == 0) //child
  {
    prctl(PR_SET_PDEATHSIG, SIGTERM);
    close(pipe_handler[0]);
    connectionMonitorization();
  }
  else //father
  {
    close(pipe_handler[1]);   
    pthread_create(&nodeCheckingThread, NULL, &ProcessMonitor::nodeCheckingRun,this);
    pthread_detach(nodeCheckingThread);
    pthread_create(&wifiConnectionThread, NULL, &ProcessMonitor::wifiConnectionRun,this);
    pthread_detach(wifiConnectionThread);
    ros::spin();
  }
}


ProcessMonitor::~ProcessMonitor()
{
  ROS_INFO("Closing ProcessMonitor");
}

bool ProcessMonitor::getProcess(std::string name, node_container** processPointer)
{
  std::vector<node_container>::iterator iterador = node_list.begin();
  while(iterador != node_list.end())
  {
    if(name.compare(iterador->name)==0)
    {
      *processPointer = &(*iterador);
      return true;
    }
    iterador++;
  }
  return false;
}

std::string ProcessMonitor::stateToString(DroneProcess::State state)
{
  std::string result;
  switch(state)
  {
    case STATE_CREATED:
      result="Created";
      break;
    case STATE_READY_TO_START:
      result="ReadyToStart";
      break;
    case STATE_RUNNING:
      result="Running";
      break;
    case STATE_PAUSED:
      result="Paused";
      break;
    case STATE_STARTED:
      result="Started";
      break;
    case STATE_NOT_STARTED:
      result="NotStarted";
      break;
    default:
      ROS_WARN("In node %s, method stateToString received a invalid State. Value received is %d.",ros::this_node::getName().c_str(),state);
      break;
  }

  return result;
}

std::string ProcessMonitor::errorToString(DroneProcess::Error error)
{
  static const char* errorArray[] =
  { 
    "UnexpectedProcessStop",
    "InvalidInputData",
    "SafeguardRecoverableError",
    "SafeguardFatalError",
  };
  std::string result;
  result.assign(errorArray[(int)error]);
  return result;
}

void ProcessMonitor::watchdogCallback(const droneMsgsROS::AliveSignal::ConstPtr& msg)
{
  node_container* container_pointer;

  //If node already stored
  if (getProcess(msg->process_name.c_str(),&container_pointer)) 
  {
    (*container_pointer).last_signal= ros::Time::now();
    (*container_pointer).current_state =(DroneProcess::State) msg->current_state.state;
    //If node was not previously alive
    if(!(*container_pointer).is_alive)
    {
      (*container_pointer).is_alive = true;
      droneMsgsROS::ProcessError error_message;
      error_message.header.stamp = ros::Time::now();
      error_message.error_type.value = DroneProcess::UnexpectedProcessStop;
      error_message.process_name = msg->process_name.c_str();
      error_message.hostname = msg->hostname.c_str();
      error_message.description = "This node has been restored. It will be monitorized again.";
      error_message.reference_code = 2;
      error_informer.publish(error_message);
      ROS_ERROR("'%s' Error:\n\tProcess name: %s\n \tHostname: %s\n \tError function: %s\n \tError description: %s\n\tError code: %d\n",
        errorToString((DroneProcess::Error)error_message.error_type.value).c_str(),error_message.process_name.c_str(),
        error_message.hostname.c_str(), error_message.function.c_str(),error_message.description.c_str(),error_message.reference_code);
    }
  }
  //If node has not be stored yet
  //TODO, Decide if we should add a state check before inserting
  else if(msg->current_state.state != STATE_CREATED)
  {
    ROS_INFO("Inserting: %s with state: %s", msg->process_name.c_str(),stateToString((DroneProcess::State)msg->current_state.state).c_str());
    node_container new_node;
    new_node.name.assign(msg->process_name.c_str());
    new_node.hostname.assign(msg->hostname.c_str());
    new_node.current_state = (DroneProcess::State) msg->current_state.state;
    new_node.last_signal = ros::Time::now();
    new_node.is_alive = true;
    node_list.push_back(new_node);
  }
}

void ProcessMonitor::errorCallback(const droneMsgsROS::ProcessError::ConstPtr& msg)
{
  ROS_ERROR("Recieved error of type '%s':\n\tProcess name: %s\n \tHostname: %s\n \tError function: %s\n \tError description: %s\n\tError code: %d\n",
    errorToString((DroneProcess::Error)msg->error_type.value).c_str(),msg->process_name.c_str(),
      msg->hostname.c_str(), msg->function.c_str(),msg->description.c_str(),msg->reference_code);
  error_informer.publish(msg);
}

void ProcessMonitor::nodeChecking()
{
  ros::Rate r(10);
  while (ros::ok())
  {
    std::vector<node_container>::iterator eraser_iterator;
    bool erase = false;
    for (std::vector<node_container>::iterator it = node_list.begin(); it != node_list.end(); ++it)
    {
      if(it->is_alive == true && ros::Time::now().toSec() - it->last_signal.toSec() > 2)
      {
        droneMsgsROS::ProcessError error_message;
        error_message.header.stamp = ros::Time::now();
        error_message.error_type.value = DroneProcess::UnexpectedProcessStop ;
        error_message.process_name = it->name.c_str();
        error_message.hostname = it->hostname.c_str();
        error_message.description = "This process is no longer sending an alive signal. It might be dead or disconnected from the network";
        error_message.reference_code = 1;
        error_informer.publish(error_message);
        ROS_ERROR("'%s' Error:\n\tProcess name: %s\n \tHostname: %s\n \tError function: %s\n \tError description: %s\n\tError code: %d\n",
          errorToString((DroneProcess::Error)error_message.error_type.value).c_str(),error_message.process_name.c_str(),
          error_message.hostname.c_str(), error_message.function.c_str(),error_message.description.c_str(),error_message.reference_code);
        it->is_alive = false;
        sendProcessStatus();
      }

      /* TODO, Define wheter processes should be deleted from the node list, even if they are dead.
      if(it->current_state == STATE_READY_TO_START)
      {
        ROS_INFO("Deleting %s from check list.\n",it->name.c_str());
        eraser_iterator=it;
        erase = true;
      }*/
    }
    
    if(erase)
    {
      erase = false;
      node_list.erase(eraser_iterator);
    }
    //Sends all processes status after recieving all processes updates.
    sendProcessStatus();
    r.sleep();
  }
}

void ProcessMonitor::sendProcessStatus()
{
  droneMsgsROS::ProcessDescriptorList process_descriptor_message;
  droneMsgsROS::ProcessDescriptor process_descriptor;
  for (int idx = 0; idx < node_list.size(); idx++)
  {
    process_descriptor.name = node_list[idx].name;
    process_descriptor.hostname = node_list[idx].hostname;
    process_descriptor.last_signal = node_list[idx].last_signal;
    process_descriptor.is_alive = node_list[idx].is_alive;
    process_descriptor.current_state.state = (int) node_list[idx].current_state;
    process_descriptor_message.process_list.push_back(process_descriptor);
  }
  process_informer.publish(process_descriptor_message);
}

bool ProcessMonitor::moduleIsStartedServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response)
{
  response.ack = false;
  for (std::vector<node_container>::iterator it = node_list.begin(); it != node_list.end() && !response.ack; ++it)
  {
    std::size_t pos = it->name.rfind("/");
    pos++;
    if(it->name.substr(pos).compare(request.module_name) == 0 )
    {
      if (it->is_alive && it->current_state == STATE_STARTED)
      {
        response.ack = true;
      }
    }
  }
  return true;
}

bool ProcessMonitor::moduleIsOnlineServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response)
{
  response.ack = false;
  for (std::vector<node_container>::iterator it = node_list.begin(); it != node_list.end() && !response.ack; ++it)
  {
    std::size_t pos = it->name.rfind("/");
    pos++;
    if(it->name.substr(pos).compare(request.module_name.c_str()) == 0 && it->is_alive)
    {
      response.ack = true;
    }
  }
  return true;
}


void * ProcessMonitor::wifiConnectionRun(void * argument)
{
  ((ProcessMonitor *) argument)->getConnectionState();
  return NULL;
}

void * ProcessMonitor::nodeCheckingRun(void * argument)
{
  ((ProcessMonitor *) argument)->nodeChecking();
  return NULL;
}

void ProcessMonitor::connectionMonitorization()
{
  //Check wifi connection for the first time
  bool old_connection_status;
  connection_status = checkConnection();
  old_connection_status = connection_status;
  write(pipe_handler[1], &connection_status, sizeof(connection_status));

  //Advertise only when connection state changes
  ros::Rate r(2);
  while(ros::ok())
  {
    connection_status = checkConnection();
    if (connection_status != old_connection_status)
    {
      write(pipe_handler[1], &connection_status, sizeof(connection_status));
    }
    old_connection_status=connection_status;
    r.sleep();
  }
}

bool ProcessMonitor::checkConnection()
{
  bool result = false;
  int status = system(ping_command.c_str());
  if (-1 != status) 
  { 
    int ping_ret = WEXITSTATUS(status); 
    if(ping_ret==0)
    {
        result = true;
    }
    return result;
  }
}

bool ProcessMonitor::connectionServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response)
{
  response.success = connection_status;
  return true;
}

bool ProcessMonitor::getConnectionState()
{
  std_msgs::Bool wifi_status_msg;
  wifi_status_msg.data = connection_status;
  ros::Rate father_thread_rate(2);
  while(ros::ok)
  {
    read(pipe_handler[0], &connection_status, sizeof(connection_status));
    wifi_status_msg.data = connection_status;
    wifi_connection_informer.publish(wifi_status_msg);
    father_thread_rate.sleep();
  }
}