/*!*******************************************************************************************
 *  \file       process_monitor_process.h
 *  \brief      ProcessMonitor definition file.
 *  \details    This file includes the ProcessMonitor class declaration. To obtain more 
 *              information about it's definition consult the process_monitor_process.cpp file.
 *  \author     Enrique Ortiz, David Palacios
 *  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM) 
 *
 *     This program is free software: you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation, either version 3 of the License, or 
 *     (at your option) any later version. 
 *   
 *     This program is distributed in the hope that it will be useful, 
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 *     GNU General Public License for more details. 
 *   
 *     You should have received a copy of the GNU General Public License 
 *     along with this program. If not, see http://www.gnu.org/licenses/. 
 ********************************************************************************/

#ifndef PROCESS_MONITOR
#define PROCESS_MONITOR

#include <signal.h>
#include <sys/prctl.h>
#include <vector>
#include <string>
#include <ros/ros.h>
#include "drone_process.h" //This file is needed to obtain the State and Error defs.
#include <droneMsgsROS/AliveSignal.h>
#include <droneMsgsROS/ProcessError.h>
#include <droneMsgsROS/ProcessDescriptorList.h>
#include <droneMsgsROS/askForModule.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Bool.h>

/*!******************************************************************************************
 *  \class      ProcessMonitor
 *  \brief      The process monitor is the node responsible of monitoring the rest of processes.
 *  \details    The ProcessMonitor has the purpose of checking if any running process has died.
 *              The ProcessMonitor learns which processes has to take care of by storing the name
 *              each process periodically sends. If a process stops sending messages through
 *              the alive_signal_topic topic, the ProcessMonitor will show a message through std_out
 *              warning about this situation. There should only be one ProcessMonitor instance
 *              running at each time.
 *********************************************************************************************/
class ProcessMonitor
{
public:
  //! Constructor. \details The ProcessMonitor constructor doesn't need any argument through argv.
  ProcessMonitor(int argc, char **argv);
  ~ProcessMonitor();

private:
  /*!***************************************************************************************
   * \struct node_container
   * \brief  This struct stores information about a process that has to monitorized.
   ****************************************************************************************/
  struct node_container
  {
    std::string name;                       //!< DroneProcess name of the ros node
    std::string hostname;                   //!< Computer hostname where runs the ProcessMonitor
    ros::Time last_signal;                  //!< Indicates the time of the last receive signal
    bool is_alive;                          //!< Indicates if the DroneProcess is alive or not 
    DroneProcess::State current_state;      //!< Current state of the DroneProcess
  };

  std::string drone_id;                     //!< Drone on which is executing the process.
  std::vector<node_container> node_list;    //!< Information about the used DroneProcess at the moment.
  
  ros::Publisher error_informer;            //!< ROS publisher handler used to send error information to HMI.
  ros::Publisher process_informer;          //!< ROS publisher handler used to send error information to HMI.
  ros::Publisher wifi_connection_informer;  //!< ROS publisher handler used to wifi status.
  std::string alive_signal_topic;           //!< Topic name to receive alive messages from DroneProcess.
  std::string error_topic;                  //!< Topic name to receive errors from DroneProcess. 
  std::string error_notification_topic;     //!< Topic name to send information to HMI.
  std::string processes_performance_topic;      //!< Topic name to send process information to HMI.
  std::string wifi_connection_topic;        //!< Topic name to send wifi status.
  std::string drone_ip_address;             //!< Ip adress to check connectivity with drone.
  std::string ping_command;                 //!< Linux dependent program execution command.

  bool connection_status;                   //!< This atrribute stores if the connection with the drone is operative or not.
  
  static void * wifiConnectionRun(void * argument);
  static void * nodeCheckingRun(void * argument);
  void connectionMonitorization();  //!< This function implements the thread's logic.
  pthread_t wifiConnectionThread;   //!< Thread handler.
  pthread_t nodeCheckingThread;     //!< Thread handler.
  int pipe_handler[2];
 
  ros::ServiceServer isConnectedSrv;
  ros::ServiceServer isStartedSrv;
  ros::ServiceServer isOnlineSrv;

  /*!***************************************************************************************
   * \details This is the method called every time a new message arrives through the 
   *          'alive_signal_topic' topic.
   * \param msg The recieved message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void watchdogCallback(const droneMsgsROS::AliveSignal::ConstPtr& msg);

  /*!***************************************************************************************
   * \details This method gets called every time a new error message arrives through the 
   *          'error_topic' topic.
   * \param msg The recieved message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void errorCallback(const droneMsgsROS::ProcessError::ConstPtr& msg);

  /*!***************************************************************************************
   * \details This function get's called from the constructor. It is a continous loop that
   *    constantly checks if it hasn't received any message from a process in the last seconds. 
   *    If so, the ProcessMonitor prints a warning through the std_out.
   * \return Void function
   ****************************************************************************************/
  void nodeChecking();

  /*!***************************************************************************************
   * \details Internal function used to find a stored process by it's name. If the process
   * is found, true is returned and the processPointer points to the process structure.
   * \param name The name of the process to find as a String.
   * \param processPointer An exit parameter used to return the found container.
   * \return If found process with the given name, true is returned and processPointer points 
   *   to the found container.
   ****************************************************************************************/ 
  bool getProcess(std::string name, node_container** processPointer);

  /*!***************************************************************************************
   * \details This function is used internally to translate the state enum code into a
   *          human readeable String.
   * \param state which is defined at the DroneProcess class.
   * \return A String representing the node state passed through the state argument
   ****************************************************************************************/ 
  std::string stateToString(DroneProcess::State state);

  /*!***************************************************************************************
   * \details This function is used to translate the error enum code into a
   *          human readeable String.
   * \param error An Error type defined at the DroneProcess class.
   * \return A string is returned representing the Error type recieved as an argument.
   ****************************************************************************************/ 
  std::string errorToString(DroneProcess::Error error);

  /*!***************************************************************************************
   * \details Sends all processes information as an array through ROS topics
   * \return Void function.
   ****************************************************************************************/ 
  void sendProcessStatus();

  /*!***************************************************************************************
   * \details Service for asking if a certain module has started.
   * \return A Boolean indicating if the service has run correctly
   ****************************************************************************************/
  bool moduleIsStartedServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response);

  /*!***************************************************************************************
   * \details Service for asking if a certain module is online.
   * \return A Boolean indicating if the service has run correctly
   ****************************************************************************************/ 
  bool moduleIsOnlineServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response);

  /*!***************************************************************************************
   * \details Service for asking if the process monitor is connected to the specified
   * IP address at the parameter server.
   * \return A Boolean indicating if the service has run correctly
   ****************************************************************************************/ 
  bool connectionServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response);
  
  /*!***************************************************************************************
   * \details Function to check if the process monitor can connect with a specified IP
   * address in the parameter server.
   * \return A Boolean indicating if the service has run correctly
   ****************************************************************************************/ 
  bool checkConnection();
  
  /*!***************************************************************************************
   * \details Gets the connection state given by the child process through the pipe and
   * sends it through the isWifiOk topic.
   ****************************************************************************************/ 
  bool getConnectionState();
};
#endif